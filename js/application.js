"use strict";

$(function () {
    locator.getLocation(function (pos) {
        if (typeof pos.error.code !== "undefined") {
            alert(pos.error.code + ' : ' + pos.error.message);
        } else {
            alert(pos.lat + ', ' + pos.lng);
        }

        console.log(pos);
    });
});
