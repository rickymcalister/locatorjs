"use strict";

(function (locator, $, undefined) {
    // Private properties
    var pos = {
        lat: 0,
        lng: 0,
        error: {}
    };

    /**
     * Private: Success callback
     *
     * @param position
     */
    function setLocation(position) {
        pos.lat = position.coords.latitude;
        pos.lng = position.coords.longitude;
    }

    /**
     * Private: Error callback
     *
     * @param error
     */
    function setError(error) {
        switch (error.code) {
            case error.PERMISSION_DENIED:
                pos.error = {
                    code: error.code,
                    message: "Request denied by user"
                };

                break;
            case error.POSITION_UNAVAILABLE:
                pos.error = {
                    code: error.code,
                    message: "Location information unavailable"
                };

                break;
            case error.TIMEOUT:
                pos.error = {
                    code: error.code,
                    message: "Request timed out"
                };

                break;
            default:
                pos.error = {
                    code: error.code,
                    message: "Unknown error"
                };

                break;
        }
    }

    /**
     * Public: Get the user's location data
     *
     * @returns {boolean}
     */
    function getLocationData() {
        if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(setLocation, setError);

            return true;
        } else {
            pos.error = {
                message: "Geo-location is not supported by this browser."
            };

            return false;
        }
    }

    /**
     * Public: Helper function for processing the location data
     *
     * The method accepts a callback function with a single argument for the pos object.
     *
     * @param callback
     */
    locator.getLocation = function (callback) {
        getLocationData();

        // Wait for the location data to be set or an error to be returned
        var timer = setInterval(function () {
            if (typeof pos.error.code !== "undefined" || pos.lat !== 0 || pos.lng !== 0) {
                clearInterval(timer);

                callback(pos);
            }
        }, 500);
    };
}(window.locator = window.locator || {}, jQuery));
